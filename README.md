# CiNAC Simulator (Anti-collision Navigation Belt for Visual Impaired People) #
This project is a simulator used in the CiNAC project. All the code used here is open-source.
The project is made by 5 students from ITA as part of MTP-02 discipline (Introduction to Engineering):

 *  Carlos Matheus (Shark, T20)
 *  Eric Moreira (Precioso, T20)
 *  Felipe Coimbra (Coimbra, T20)
 *  Igor Ribeiro (Dono, T20)
 *  Luis Holanda (Mega, T20)

## Requirements and Installing: 
The simulator was developed in Ubuntu 14.01 platform. For any other Operational System the program is not garanteed to work properly. For Linux-based systems, the steps regarding dependecies installing may vary.

* Initially, it is necessary to install SFML Graphical Library
```sh
$ sudo apt-get install libsfml-dev
```
** PS.: Take note that the SFML Library requires its own dependecies. For more information refer to the official website https://www.sfml-dev.org/**

** PS.2:Make sure the libraries have been allocated in the /usr/lib/ folder as the root location.**

* To be able to compile the project, you need CMake. It may be installed automatically by
```sh
$ sudo apt-get install cmake
```
It is required a minimal version of CMake 3.5 to build the project. If apt-get doesn't install a sufficiently new version, please refer to the official website https://cmake.org/ to a newer version.

To check your CMake version, just type
```
$ cmake --version
```

## Building the project:
* If everything is in its place and you have already cloned the repository, you may set the building process simply by opening the created "Simulador" folder in console and typing
```sh
$ cmake .
```
* This will automatically generate a Makefile file. To finally build the project and execute the program just invoke make and execute the binary
```sh
$ make
$ ./bin/CiNAC_simulator
```