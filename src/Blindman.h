/*
 *      Created by Felipe Coimbra on 5/25/2017
 */

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>

#include "geom/Vector2D.h"

class Sensor;

class Blindman {
private:

    //
    //  Graphical shape object
    //
    sf::CircleShape blindShape;

    //
    //  Radius of the Blindman
    //  Local position of the Blindman's center
    //
    float radius;
    cinac::Vector2D<float> center;

    //
    //  Array of sensors,
    //      position 2 refers to the IR sensor
    //
    std::vector<Sensor> sensors;


public:

    std::vector<sf::CircleShape > debugPoints;

    //
    //  Basic constructor, receives blindman's radius and sensors' radius
    //
    Blindman(float blindR, float sensorR);

    ~Blindman();

    sf::CircleShape getShape() const{ return blindShape; }

    std::vector<Sensor> & sensorArray() { return sensors; }

    void updateSensors(sf::RenderWindow * w, std::vector<sf::Sprite> &obstacles, std::vector<float> *newDistances);

    //
    //  Shape get methods
    //

    /*
     *  Returns position of object relative to the window
     */
    sf::Vector2f position() const;

    sf::FloatRect localBounds() const;

    sf::FloatRect globalBounds() const;

    //
    //  Shape modification methods
    //

    void translate(sf::Vector2f newPosition);

    void rotate(bool clockwise);
};
