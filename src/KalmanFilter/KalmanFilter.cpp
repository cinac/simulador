//
// Created by Carlos Matheus Barros da Silva on 24/05/17.
//

#include "KalmanFilter.h"

KalmanFilter::KalmanFilter(){

    /* These initials values are arbitrary values that better fits to the problem
     * So they must be adjusted to better fit in problem */

    values = new KalmanDef;
    values->setA(1);
    values->setB(0);
    values->setH(1);
    values->setQ(0);
    values->setR(0.1);
    values->setPreviousEstimation(0);
    values->setPreviousError(1);
}

KalmanFilter::~KalmanFilter(){

}

double KalmanFilter::filter(double currentMeansuredValue){

    values->setCurrentMeasuredValue( currentMeansuredValue );

    prediction.projectStateAhead(values);
    prediction.projectErrorAhead(values);

    filterUpdater.computeKalmanGain(values);
    estimation = filterUpdater.updateEstimation(values);
    filterUpdater.upadteErrorCovariance(values);

    return estimation;
}


