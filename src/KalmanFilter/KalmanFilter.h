//
// Created by Carlos Matheus Barros da Silva on 24/05/17.
//
#ifndef CINAC_SIMULATOR_KALMANFILTER_H
#define CINAC_SIMULATOR_KALMANFILTER_H
#include "KalmanDef.h"
#include "Filter.h"
#include "Prediction.h"
#include "KalmanFilterTester.h"

class KalmanFilter {

public:
    KalmanFilter();
    ~KalmanFilter();
    double filter(double currentMeasuredValue);
private:
    KalmanDef *values;
    Filter filterUpdater;
    Prediction prediction;
    double estimation;

};

#endif //CINAC_SIMULATOR_KALMANFILTER_H
