//
// Created by Carlos Matheus Barros da Silva on 24/05/17.
//

#ifndef CINAC_SIMULATOR_PREDICTION_H
#define CINAC_SIMULATOR_PREDICTION_H
#include <iostream>
#include "KalmanDef.h"

class Prediction {

public:
    Prediction ();
    ~Prediction();
    void projectStateAhead (KalmanDef *values);
    void projectErrorAhead (KalmanDef *values);

private:
    double xk;
    double a;
    double b;
    double uk;
    double xk1;
    double pk;
    double pk1;
    double at;
    double q;
};


#endif //CINAC_SIMULATOR_PREDICTION_H
