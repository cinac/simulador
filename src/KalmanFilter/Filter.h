//
// Created by Carlos Matheus Barros da Silva on 24/05/17.
//

#ifndef CINAC_SIMULATOR_FILTER_H
#define CINAC_SIMULATOR_FILTER_H


#include "KalmanDef.h"

class Filter {
public:
    Filter();
    ~Filter();
    void computeKalmanGain(KalmanDef *values);
    double updateEstimation(KalmanDef *values);
    void upadteErrorCovariance(KalmanDef *values);
private:
    double kk;
    double pk;
    double h;
    double ht;
    double r;
    double xk;
    double zk;
    double xk1;
    double xkCurrent;
    double pkCurrent;
    double i;
};


#endif //CINAC_SIMULATOR_FILTER_H
