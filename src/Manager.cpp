//
// Created by igor on 23/05/17.
//

#include <algorithm>
#include "Manager.h"
#include "Sensor.h"
#include "Processador.h"

//#define DEBUG_MOUSE
//#define DEBUG_BLINDMAN
#define DEBUG_SENSOR_TARGETS
#define DEBUG_RAYS
#define DEBUG_OBSTACLES

Manager::Manager(sf::RenderWindow *W, sf::RenderWindow *auxW)
    : window(W),
      auxWindow(auxW),
      percentages(4,0),
      font(),
      obstacles( new Obstacles()),
      walker( new Blindman(20,2)),
      waitTime(0)
{

    // Adjusts size of auxiliary window
    //auxWindow->setSize(sf::Vector2u(400,150));


    // Initializes Font-Type
    if(!font.loadFromFile("../res/fonts/arial.ttf")){
        std::cout << "Could not load font style." << std::endl;
        window->close();
    }

    updateGraphics();
}

Manager::~Manager() {

}

void Manager::run() {

    // run the program as long as the window is open
    while (window->isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while ( window->pollEvent(event) )
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window->close();
                //  window is on. See what's the new user's input
            else
                handleUserInput();

            updateWaitTime(0);
        }
    }
}

void Manager::updateWaitTime(int state)
{
    if(state == 0)
        waitTime = 0;
    else if(waitTime>0)
        waitTime--;
}

// Analyses mouse entry, changes walker and the percentages properties
void Manager::handleUserInput()
{
    bool firstClicked = false;
    bool leftClicked = false;
    bool rightClicked = false;
    bool collided = false;
    sf::Vector2f positionClicked;

    // User clicked! Time to work
    while(sf::Mouse::isButtonPressed(sf::Mouse::Left) |
            (leftClicked = sf::Keyboard::isKeyPressed(sf::Keyboard::A), rightClicked = sf::Keyboard::isKeyPressed(sf::Keyboard::D)))
    {
        // Get mouse's local position (with respect to the window)
        sf::Vector2i mousePosition = sf::Mouse::getPosition(*window);

        // Calculates mouse's position in the coordinate system fixed in the walker(WithRespectToblind)
        sf::Vector2i mousePositionWRTblind((int)(mousePosition.x-walker->position().x),
                                           (int)(mousePosition.y-walker->position().y));

        // Get blind local bounds( in the coordinate system fixed in the walker)
        sf::FloatRect walkerBounds = walker->localBounds();

#ifdef DEBUG_MOUSE
        std::cout << mousePosition.x << "  " << mousePosition.y << std::endl;
        //std::cout << mousePositionWRTblind.x << "  " << mousePositionWRTblind.y << std::endl;
#endif

#ifdef DEBUG_BLINDMAN
        //std::cout <<"("<<walkerBounds.left<<" , " << "  " << walkerBounds.top<<")" << std::endl;
        //std::cout <<"("<<walkerBounds.width<<" , " << "  " << walkerBounds.height<<")" << std::endl;
#endif
        // if mousePosition is inside blind bounds
        if(walkerBounds.contains(sf::Vector2f(mousePositionWRTblind)) || firstClicked){
            // For first click
            if(!firstClicked){

                firstClicked = true;
                // Saves position clicked(WRTblind)
                positionClicked = sf::Vector2f(mousePosition.x-walker->position().x,
                                               mousePosition.y-walker->position().y);
            }

            //New position of the walker
            sf::Vector2f newPosition = sf::Vector2f(mousePositionWRTblind) - positionClicked;

            // Update Blindman's position and display it
            if(!collided)
                collided = !translateBlindmanPosition(newPosition);

            if(rightClicked)
                rotateBlindmanPosition(true); //clockwise rotate

            if(leftClicked)
                rotateBlindmanPosition(false); //anti-clockwise rotate

            // Updates percentages vector
            updatePercentages();
        }

        // Update Wait Time
        // Note: This update is on-click-time. General update is made on .run() method
        if(!rightClicked && !leftClicked)
            updateWaitTime(1);

        // Display graphical windows
        // Note: Only called when mouse is clicked for optimizations purposes
        updateGraphics();

    }
}

bool Manager::translateBlindmanPosition(sf::Vector2f newPosition)
{
    walker->translate(newPosition);

    //Tests if walker new position is going to intersect one of the obstacles->walls
    bool intersect = false;
    for(int i=0; i<(int)obstacles->walls.size(); i++)
        if(walker->globalBounds().intersects(obstacles->walls[i].getGlobalBounds()))
            intersect = true;

    //If it is in the bounds of a wall, goes back
    if(intersect) {
        walker->translate(-newPosition);
        return false;
    }

    return true;
}

void Manager::rotateBlindmanPosition(bool clockwise)
{
    if(waitTime == 0) {
        walker->rotate(clockwise);
        waitTime = 5;
    }

}

void Manager::updatePercentages()
{
    std::vector<float> *distances( new std::vector<float>() );

    walker->updateSensors(window,obstacles->walls, distances);

    std::vector<float> vibrations = Processador::vibrations(distances);

    percentages.clear();
    for(int i=0; i<(int)distances->size(); i++)
        percentages.push_back( vibrations[i] );
}

void Manager::updateGraphics()
{
    std::string text;

    for(int i=0; i<5; i++)
        text = text + std::to_string(percentages[i]) + (i==3?"%":"% ");

    drawWindow();
    drawAuxWindow(text);

    window->display();
    auxWindow->display();
}

void Manager::drawWindow() {
    window->clear(sf::Color::Black);

    window->draw(walker->getShape());

    for (int i = 0; i < (int) walker->sensorArray().size(); i++)
        window->draw(walker->sensorArray()[i].getShape());

    for (int i = 0; i < (int) obstacles->walls.size(); i++)
        window->draw(obstacles->walls[i]);

#ifdef DEBUG_SENSOR_TARGETS
    for (int i = 0; i < (int) walker->debugPoints.size(); i++)
        if (walker->sensorArray()[i].position()-sf::Vector2f(walker->debugPoints[i].getRadius(),walker->debugPoints[i].getRadius()) !=
                                    walker->debugPoints[i].getPosition())
            window->draw(walker->debugPoints[i]);
#endif

#ifdef DEBUG_RAYS
    if(sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
        for (int i = 0; i < (int) walker->sensorArray().size(); i++) {
//            std::cout << "Origin = " << "(" << walker->sensorArray()[i].getRay().getOrigin().getX() << " , "
//                      << walker->sensorArray()[i].getRay().getOrigin().getY() << ")"
//                      << std::endl;
//            std::cout << "Direction = " << "(" << walker->sensorArray()[i].getRay().getFullLine().getDir().getX() << " , "
//                      << walker->sensorArray()[i].getRay().getFullLine().getDir().getY() << ")"
//                      << std::endl;
            sf::Vector2f ori(walker->sensorArray()[i].getRay().getOrigin().getX() - 2,
                             walker->sensorArray()[i].getRay().getOrigin().getY() - 2);

            sf::CircleShape origin(2, 80);
            origin.setFillColor(sf::Color::White);
            origin.setPosition(ori);
            window->draw(origin);

            sf::Vector2f step((walker->sensorArray()[i].getRay().getFullLine().getDir().normalize() * 10).getX(),
                              (walker->sensorArray()[i].getRay().getFullLine().getDir().normalize() * 10).getY());
            for (int i = 1; i <= 40; i++) {
                origin.setPosition(ori.x + i * step.x, ori.y + i * step.y);
                window->draw(origin);
            }
        }
    }

#endif

#ifdef DEBUG_OBSTACLES
    for(int i=0; i<(int)obstacles->debugObst.size(); i++)
        for(int j=0; j<(int)obstacles->debugObst[i].size(); j++)
            window->draw(obstacles->debugObst[i][j]);
#endif
}


void Manager::drawAuxWindow(std::string &newText)
{
    sf::Text textToDisplay(newText,font,40);
    textToDisplay.move(20,20);

    auxWindow->clear(sf::Color::Black);
    auxWindow->draw(textToDisplay);
}