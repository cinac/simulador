//
// Created by luiscm on 02/06/17.
//

#ifndef CINAC_SIMULATOR_PROCESSAMENTO_H
#define CINAC_SIMULATOR_PROCESSAMENTO_H


#include <vector>
#include "KalmanFilter/KalmanFilter.h"

class Processador
{
public:
    /*
     * Return the vibrations for all the sensors given the
     * distances for the objects.
     */
    static std::vector<float> vibrations(std::vector<float> * inDistances);

private:
    /*
     * Return the vibration of the sensor given the distance
     */
    static float _get_vibration(float distance);

    /*
     * Add errors to a distance and return the new distance added with erros
     */
    static float addErros(float distance, int index);

    static KalmanFilter kalmanFilter[5];

    static float filter(float newDistance, int index);
};

#endif //CINAC_SIMULATOR_PROCESSAMENTO_H
