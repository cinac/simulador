#include "Obstacles.h"
#include "Sensor.h"
#include "Blindman.h"
#include "Polygon2D.h"

#define DEBUG_SENSOR_TARGETS  //AZUL ESCURO

Sensor::Sensor(cinac::Vector2D<float> blindcenter, cinac::Vector2D<float> pos, float r)
:   localPosition( pos ),
    blindCenter(blindcenter),
    shapeRadius(r),
    ray(localPosition,(localPosition-blindCenter).normalize()),
    dist(INF),
    graphicalUtil(shapeRadius,80)
{
    graphicalUtil.setFillColor(sf::Color::Green);
    graphicalUtil.setPosition(sf::Vector2f(localPosition.getX()-shapeRadius, localPosition.getY()-shapeRadius) );
}

Sensor::~Sensor()
{

}

void Sensor::translate(sf::Vector2f displacement)
{
    blindCenter += cinac::Vector2D<float>(displacement);
    localPosition += cinac::Vector2D<float>(displacement);
    updateRay();
    graphicalUtil.move(displacement);
}

void Sensor::rotate(cinac::Vector2D<float> pivot, bool clockwise)
{
    double theta = clockwise?-90:90;
    cinac::Vector2D<float> previousPosition = localPosition;
    localPosition = cinac::Vector2D<float>::rotateAroundOrigin(previousPosition,pivot,theta);
    updateRay();

    cinac::Vector2D<float> displacement = localPosition-previousPosition;
    graphicalUtil.move( sf::Vector2f(displacement.getX(),displacement.getY()) );
}

void Sensor::updateRay()
{
    ray = cinac::Ray2D(localPosition, (localPosition-blindCenter).normalize());
}

float Sensor::sendRay(sf::RenderWindow * w, std::vector<cinac::Polygon2D> &obstacles, int sensor, Blindman* blindman)
{
    sf::Vector2f closestIntersec(-1,-1);
    float minDist = INF;

    for(int i=0; i<(int)obstacles.size(); i++) {
#ifndef DEBUG_SENSOR_TARGETS
        std::vector<cinac::Vector2D<float> > intersec = obstacles[i].intersection(ray);
#else
        std::vector<cinac::Vector2D<float> > intersec;
        if(sensor==sensor) {
            intersec = obstacles[i].intersection(ray);
            // std::cout << "Found " << intersec.size() << " intersections" << std::endl;
        }

#endif
        int closest=-1;
        float closestDist=INF+10;
        for(int j=0; j<(int)intersec.size(); j++)
            if(intersec[j].distance(localPosition) < closestDist){
                closest = j;
                closestDist = intersec[j].distance(localPosition);
            }
#ifdef DEBUG_SENSOR_TARGETS
        if(sensor==sensor)
        //std::cout << "closest = " << closest << std::endl;
#endif

        if(closestDist<minDist) {
            minDist = closestDist;
            closestIntersec = sf::Vector2f(intersec[closest].getX(),intersec[closest].getY());
        }
    }

    blindman->debugPoints[sensor].setPosition(sf::Vector2f(localPosition.getX()-blindman->debugPoints[sensor].getRadius(),
                                                           localPosition.getY()-blindman->debugPoints[sensor].getRadius())
                                              );
    if(closestIntersec != sf::Vector2f(-1,-1) && sensor==sensor)
        blindman->debugPoints[sensor].setPosition(closestIntersec.x-blindman->debugPoints[sensor].getRadius(),
                                                  closestIntersec.y-blindman->debugPoints[sensor].getRadius());

    dist = minDist;

#ifdef DEBUG_SENSOR_TARGETS
    //std::cout << "minimal distance = " << dist << std::endl;
    if(sensor==sensor)
    //std::cout << std::endl;
#endif

    return dist;
}