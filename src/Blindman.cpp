//
// Created by Felipe Coimbra on 5/25/2017
//
#include <vector>
#include "Blindman.h"
#include "Sensor.h"
#include "geom/Polygon2D.h"

//#define DEBUG_OBSTACLES
//#define DEBUG_SENSOR_DIST
#define DEBUG_SENSOR_TARGETS

Blindman::Blindman(float blindR, float sensorR)
    :   radius(blindR),
        blindShape(blindR,80),
        sensors()
{
    blindShape.setFillColor(sf::Color::Red);

    // Center position of the Blindman shape
    center = cinac::Vector2D<float> (this->position()+sf::Vector2f(radius,radius));

    // Position of first sensor: -60 degrees
    cinac::Vector2D<float> sensorPoint = center + cinac::Vector2D<float>(1/2.0, sqrt(3.0)/2.0)*blindR;


    // Adds sensor points rotated by 30 degrees
    for(int i=0; i<5; i++){
        sensors.push_back( Sensor(center, sensorPoint, sensorR) );

        sensorPoint = cinac::Vector2D<float>::rotateAroundOrigin(sensorPoint,center,30);
    }

    //sensors.push_back( Sensor(this->position(), this->position(), sensorR,w) );
#ifdef DEBUG_SENSOR_TARGETS

    debugPoints.resize(sensors.size());
    for(int i=0; i<(int)debugPoints.size(); i++) {
        sf::CircleShape base(3,80);
        base.setFillColor(sf::Color::Blue);
        base.setPosition(sensors[i].getShape().getPosition());

        debugPoints[i] = base;
    }
#endif

}

Blindman::~Blindman()
{

}


sf::Vector2f Blindman::position() const
{
    return blindShape.getPosition();
}

sf::FloatRect Blindman::localBounds() const
{
  return blindShape.getLocalBounds();
}

sf::FloatRect Blindman::globalBounds() const
{
  return blindShape.getGlobalBounds();
}

void Blindman::translate(sf::Vector2f displacement)
{
    center += cinac::Vector2D<float>(displacement);

    blindShape.move(displacement);

    for(int i=0; i<(int)sensors.size(); i++)
        sensors[i].translate(displacement);

    return;
}

void Blindman::rotate(bool clockwise)
{
    for(int i=0; i<(int)sensors.size(); i++)
        sensors[i].rotate(center, clockwise);
}

void Blindman::updateSensors(sf::RenderWindow * w, std::vector<sf::Sprite> &obstacles, std::vector<float> *newDistances)
{
    std::vector<cinac::Polygon2D> polyObsts;
    for(int i=0; i<(int)obstacles.size(); i++) {
        polyObsts.push_back(cinac::Polygon2D::toPoly(obstacles[i].getPosition(), obstacles[i].getLocalBounds()));
    }

#ifdef DEBUG_OBSTACLES
    /* std::cout << "Created obstacle: "<<std::endl <<
               "("<< polyObsts[i].getVert()[0].getX() << ","<< polyObsts[i].getVert()[0].getY() << ")" << std::endl <<
               "("<< polyObsts[i].getVert()[1].getX() << ","<< polyObsts[i].getVert()[1].getY() << ")" << std::endl <<
               "("<< polyObsts[i].getVert()[2].getX() << ","<< polyObsts[i].getVert()[2].getY() << ")" << std::endl <<
               "("<< polyObsts[i].getVert()[3].getX() << ","<< polyObsts[i].getVert()[3].getY() << ")" << std::endl;
*/
#endif

    for(int i=0; i<(int)sensors.size(); i++) {

        newDistances->push_back(sensors[i].sendRay(w, polyObsts, i, this));
#ifdef DEBUG_SENSOR_DIST
        std::cout << "Sensor "<<i<<": " << sensors[i].getDist();
#endif
    }
#ifdef DEBUG_SENSOR_DIST
    std::cout << std::endl<<std::endl;
#endif

    return;
}