//
// Created by luiscm on 02/06/17.
//

#include <vector>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include "Processador.h"


KalmanFilter Processador::kalmanFilter[5] = {KalmanFilter::KalmanFilter(),KalmanFilter::KalmanFilter(),KalmanFilter::KalmanFilter(),KalmanFilter::KalmanFilter(),KalmanFilter::KalmanFilter() };

std::vector<float, std::allocator<float>> Processador::vibrations(std::vector<float> *inDistances) {
    std::vector<float> _vibrations{};

    _vibrations.resize((*inDistances).size());

    for (int i = 0; i < _vibrations.size(); ++i) {
        (*inDistances)[i] = addErros((*inDistances)[i], i);
        if (i != 2)
            _vibrations[i] = _get_vibration((*inDistances)[i]);
        else {
            /*
             * The middle sensor is an IR sensor. It will vibrate only
             * when the distance is less than 30mm because the specifications
             * of the sensor used.
             */
            if ((*inDistances)[i] <= 30)
                _vibrations[i] = 100;
            else _vibrations[i] = 0;
        }

    }
    return _vibrations;
}

float Processador::_get_vibration(float distance) {
    /*
     * The function v(x) that defines the vibration is:
     *
     *          v(x) := 100(1 - exp(Bt)),
     *
     *  where
     *
     *          t = 1/(500^D) - 1/(x^D),
     *
     *  and B, D are constants to be determined.
     */

    static const float NAPIER = 2.718281;

//    Function constants
    static const float B = 5e3;
    static const float D = 2.2;

    float exp;

    if (distance <= 500)
        exp = B * (1 / powf(500, D) - 1 / powf(distance, D));
    else
        exp = 0;

    return 100 * (1 - powf(NAPIER, exp));
}

float Processador::filter(float newDistance, int index){
    printf("nao filtrado = %f \n",newDistance);
    printf("flitrado = %f \n", (float) kalmanFilter[index].filter(newDistance));
    return (float) kalmanFilter[index].filter(newDistance);
}

float Processador::addErros(float distance, int index) {
    float multiplierValue = 20*distance/100; // the constant that the error needs to be multiplied
    float error = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
    error *= multiplierValue;
    error -= multiplierValue/2;
    float newDistance = distance + error;

    float a = filter(newDistance, index);

    printf("flitrado a = %f \n", a);

    return a;
}

