#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>

class Obstacles {
public:

  Obstacles();

  ~Obstacles();

  std::vector< sf::Sprite > createWalls(sf::Sprite wall);

  std::vector<std::vector<sf::CircleShape> > debugObst;

  sf::Texture wallTexture;
  sf::Sprite wallSprite;
  std::vector< sf::Sprite > walls;

};
