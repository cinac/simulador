//
// Created by Felipe Coimbra on 5/19/17.
//

#include "Vector2D.h"
#include "Line2D.h"
#include "SegLine2D.h"

//#define DEBUG

namespace cinac {

    bool Line2D::accEqual(float coeff, float value) const {
        return fabs(coeff - value) < EPS;
    }

    Line2D& Line2D::operator = (const Line2D &other)
    {
        this->a = other.getA();
        this->b = other.getB();
        this->c = other.getC();
        this->m = other.getM();
        this->n = other.getN();
        this->refPoint = other.getRefPoint();
        this->dirVec = other.getDir();

        return *this;
    }

    Line2D::Line2D(const Line2D &other)
    {
        *this = other;
    }

    Line2D::Line2D(float newa, float newb, float newc)
            : a(newa), b(newb), c(newc)
    {
        if (accEqual(newa, 0) && accEqual(newb, 0)) {
            std::cout << "WARNING: Tried to force invalid Line2D intialization" << std::endl;
            this->~Line2D();
        }
        else if (accEqual(b, 0)) {
            m = n = 0;
            vertical = true;

            dirVec = Vector2D<float >(0, 1);
            refPoint = Vector2D<float >(0, -c / a);
        }
        else {
            m = -a / b;
            n = -c / b;

            dirVec = Vector2D<float >(-c / a, -c / b);
            refPoint = Vector2D<float >(0, -c / b);
        }
    }

    Line2D::Line2D(float newm, float newn)
            : m(newm), n(newn), vertical(false),
              a(-m), b(1), c(-n),
              refPoint(0, n)
    {
        if (accEqual(newm, 0))
            dirVec = Vector2D<float >(1, 0);
        else
            dirVec = Vector2D<float >(-n / m, n);
    }

    Line2D::Line2D(cinac::Vector2D<float > A, cinac::Vector2D<float > B)
            : dirVec(B - A),
              refPoint(A)
    {
        vertical = accEqual(A.getX(),B.getX());

        if(vertical){
            m = n = 0;
            b = 0;
            a = 1;
            c = -A.getX();
        }
        else{
            m = dirVec.getY()/dirVec.getX();
            n = A.getY() - m*A.getX();
            a = -m;
            b = 1;
            c = -n;
        }
    }

    Line2D::~Line2D() {

    }


    bool Line2D::operator==(const Line2D &other) const {
        return coincidentTo(other);
    }

    bool Line2D::parallelTo(const Line2D &other) const {
        return accEqual( Vector2D<float>::crossProd(dirVec,other.getDir()) , 0 );
    }

    bool Line2D::coincidentTo(const Line2D &other) const {
        return parallelTo(other) && accEqual(n, other.getN());
    }

    bool Line2D::contains(const Vector2D<float > &point) const {
        return accEqual(a * point.getX() + b * point.getY() + c, 0);
    }

    Vector2D<float > *Line2D::intersection(const Line2D &other) const {
        if ( parallelTo(other) || coincidentTo(other) ) {
            return nullptr;
        }

        float intersecX = (-c*other.getB() + other.getC()*b)/(a*other.getB() - other.getA()*b);
        float intersecY = (-a*other.getC() + other.getA()*c)/(a*other.getB() - other.getA()*b);

        return (new Vector2D<float >(intersecX, intersecY) );
    }

    Vector2D<float > *Line2D::intersection(const SegLine2D &other) const {
        Vector2D<float > *intersec = intersection(other.getExtrapoLine());

        if(intersec != nullptr && other.contains(*intersec))
            return intersec;

        else
            return nullptr;

    }

}
