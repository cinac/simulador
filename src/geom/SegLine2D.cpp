//
// Created by Felipe Coimbra on 5/25/17.
//

#include "Vector2D.h"
#include "Line2D.h"
#include "SegLine2D.h"

namespace cinac {

    SegLine2D::SegLine2D(cinac::Vector2D<float> A, cinac::Vector2D<float> B)
            : extrapolation(A, B),
              endpointA(A),
              endpointB(B),
              length((B - A).abs()) {
    }


    SegLine2D::~SegLine2D() {
#ifdef DEBUG
        std::cout << "WARNING: Initialized empty SegLine2D" << std::endl;
#endif
    }

    bool SegLine2D::contains(const Vector2D<float> &point) const {
        return extrapolation.contains(point) &&
               Vector2D<float>::innerProd(endpointA - point, endpointB - point) < 0;
    }

    Vector2D<float> *SegLine2D::intersection(const Line2D &other) const {

        Vector2D<float> *lineIntersec = extrapolation.intersection(other);
        if (!lineIntersec && contains(*lineIntersec)) {
            return lineIntersec;
        } else {
            return nullptr;
        }
    }

    Vector2D<float> *SegLine2D::intersection(const SegLine2D &other) const {
        Vector2D<float> *lineIntersec = intersection(other.getExtrapoLine());
        if (!lineIntersec && other.contains(*lineIntersec)) {
            return lineIntersec;
        } else {
            return nullptr;
        }
    }
}