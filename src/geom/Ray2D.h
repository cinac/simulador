//
// Created by Felipe Coimbra on 5/25/17.
//

#ifndef CINAC_SIMULATOR_RAY_H
#define CINAC_SIMULATOR_RAY_H

#include "Line2D.h"


namespace cinac {

    class Polygon2D;

    class Ray2D{

    private:
        Line2D fullLine;
        Vector2D<float> origin;
        

    public:

        Ray2D(Vector2D<float> ori, Line2D line);

        Ray2D(Vector2D<float> ori, Vector2D<float> dir);

        ~Ray2D();

        Ray2D &operator = (const Ray2D &other);

        Vector2D<float> getOrigin() const { return origin; }

        Line2D getFullLine() const { return fullLine; }

        bool
        contains(Vector2D<float> point) const;

        Vector2D<float>*
        intersection(Line2D line) const;

        Vector2D<float>*
        intersection(SegLine2D segline) const;

    };
}


#endif //CINAC_SIMULATOR_RAY_H
