//
// Created by root on 5/31/17.
//

#ifndef CINAC_SIMULATOR_POLYGON2D_H
#define CINAC_SIMULATOR_POLYGON2D_H

#include "SegLine2D.h"

namespace cinac {

    class Ray2D;

    class Polygon2D {

    private:
        //
        //  Vertices List: list of points in clockwise/counterclockwise direction
        //  Note: the last element is equal to the first one
        //
        std::vector<Vector2D<float> > vert;

        //
        //  Polygon sides list: list of segments in clockwise/counterclockwise direction
        //
        std::vector<SegLine2D > sides;

    public:
        Polygon2D(std::vector<Vector2D<float> > &points);
        ~Polygon2D();

        const std::vector<Vector2D<float> >& getVert() const { return vert; }

        const std::vector<SegLine2D >& getSides() const { return sides; }

        //
        //  Returns an array of intersection points between this polygon and the ray
        //
        std::vector<Vector2D<float> > intersection(Ray2D ray);

        //
        //  Converts the sf::FloatRect polygon to the cinac::Polygon2D data structure
        //
        static
        Polygon2D toPoly(sf::Vector2f pos, sf::FloatRect rect);

    };

}
#endif //CINAC_SIMULATOR_POLYGON2D_H
