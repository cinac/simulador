//
// Created by Felipe Coimbra on 5/19/17.
//
#ifndef CINAC_SIMULATOR_LINE2D_H
#define CINAC_SIMULATOR_LINE2D_H

#include <cmath>
#include <iostream>
#include "Vector2D.h"

//#define DEBUG

namespace cinac {

    class SegLine2D;

    class Line2D {
    private:
        // For general equation form: ax + by + c = 0
        // Note: a == b == c == 0 indicates invalid line
        float a;
        float b;
        float c;

        // For simplified equation form: y = mx + n
        // Careful with vertical lines
        float m;
        float n;
        bool vertical;

        // For vectorial treatment
        Vector2D<float > dirVec;
        Vector2D<float > refPoint;

    protected:
        // For precision handling
        const float EPS = 1e-4;

        // For accurate checking
        bool accEqual(float coeff, float value) const;

    public:

        /*
         *  Constructors // Destructors
         */

        // Primitive constructor, intializes no meaningful line
        Line2D(const Line2D & other);

        virtual ~Line2D();

        // Constructs from general equation ax + by = c
        Line2D(float newa, float newb, float newc);

        // Constructs from simplified equation y = mx + n
        // May never initialize vertical line
        Line2D(float newm, float newn);

        // Constructs from two points A and B
        Line2D(cinac::Vector2D<float > A, cinac::Vector2D<float > B);

        /*
         * Get Methods
         */
        float getA() const { return a; }

        float getB() const { return b; }

        float getC() const { return c; }

        float getM() const { return m; }

        float getN() const { return n; }

        bool isVertical() const { return vertical; }

        Vector2D<float > getDir() const { return dirVec; }

        Vector2D<float > getRefPoint() const { return refPoint; }

        /*
         *  Operators between lines
         */
        bool operator==(const Line2D &other) const;

        Line2D &operator = (const Line2D &other);

        /*
         *  Operations with other lines
         */
        // Returns true if parallel to other
        bool parallelTo(const Line2D &other) const;

        // Returns true if coincident to other
        bool coincidentTo(const Line2D &other) const;

        // Returns true if line contains point
        bool contains(const Vector2D<float > &point) const;

        // Returns intersection point between two lines
        Vector2D<float > *intersection(const Line2D &other) const;

        // Returns intersection point between a line and a segment
        Vector2D<float > *intersection(const SegLine2D &other) const;

    };

}


#endif