//
// Created by root on 5/31/17.
//

#include "Polygon2D.h"
#include "Ray2D.h"

#define DEBUG_RAY2D

namespace cinac{

    Polygon2D::Polygon2D(std::vector<Vector2D<float> > &points)
        :   vert(points)
    {
        if(vert.front() != vert.back()) {
            vert.push_back(vert.front());
            //std::cout << "duplicou os pontos: total de " << vert.size() << std::endl;
        }
        for(int i=0; i<((int)vert.size())-1; i++)
            sides.push_back( SegLine2D(vert[i], vert[i+1]) );
        //std::cout << "temos " << sides.size() << "lados" << std::endl;
        //for(int i=0; i<(int)sides.size(); i++)
            //std::cout << sides[i].get
    }

    Polygon2D::~Polygon2D()
    {

    }

    std::vector<Vector2D<float> > Polygon2D::intersection(Ray2D ray)
    {
        std::vector<Vector2D<float> > intersecList;

        for(int i=0; i<(int)sides.size(); i++){
            Vector2D<float> *newIntersec = ray.intersection(sides[i]);
            if(newIntersec != nullptr)
                intersecList.push_back(*newIntersec);
        }

        return intersecList;
    }

    Polygon2D Polygon2D::toPoly(sf::Vector2f pos, sf::FloatRect rect)
    {
        std::vector<Vector2D<float> > list;

        Vector2D<float> basePoint(pos);

        list.push_back(basePoint);
        list.push_back(basePoint + Vector2D<float>(0,rect.height));
        list.push_back(basePoint + Vector2D<float>(rect.width,rect.height));
        list.push_back(basePoint + Vector2D<float>(rect.width,0));

        return Polygon2D(list);
    }
}