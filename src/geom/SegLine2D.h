//
// Created by Felipe Coimbra on 5/25/17.
//

#ifndef CINAC_SIMULATOR_SEGLINE2D_H
#define CINAC_SIMULATOR_SEGLINE2D_H

#include "Line2D.h"

namespace cinac {

    class SegLine2D {

    private:
        const Vector2D<float> endpointA;
        const Vector2D<float> endpointB;
        const float length;
        const Line2D extrapolation;
    public:

        // Constructs Segment Line from its endpoints
        SegLine2D(cinac::Vector2D<float> A, cinac::Vector2D<float> B);

        ~SegLine2D();

        Line2D getExtrapoLine() const { return extrapolation; }

        Vector2D<float> getPointA() const { return endpointA; }
        Vector2D<float> getPointB() const { return endpointB; }

        bool contains(const Vector2D<float> &point) const;

        // Returns intersection between this SegLine and a Line2D
        Vector2D<float> *intersection(const Line2D &other) const;

        // Returns intersection between this SegLine and another SegLine
        Vector2D<float> *intersection(const SegLine2D &other) const;
    };
}


#endif //CINAC_SIMULATOR_SEGLINE2D_H
