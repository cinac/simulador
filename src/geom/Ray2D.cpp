//
// Created by Felipe Coimbra on 5/25/17.
//

#include <algorithm>
#include "Vector2D.h"
#include "SegLine2D.h"
#include "Ray2D.h"
#include "Polygon2D.h"

namespace cinac{

    Ray2D::Ray2D(Vector2D<float> ori, Line2D line)
        :fullLine(line),
         origin(ori)
    {

    }

    Ray2D::Ray2D(Vector2D<float> ori, Vector2D<float> dir)
        :origin(ori),
         fullLine(ori, ori+dir)
    {

    }

    Ray2D::~Ray2D()
    {

    }

    Ray2D & Ray2D::operator = (const Ray2D &other)
    {
        this->origin = other.getOrigin();
        this->fullLine = other.getFullLine();

        return *this;
    }

    bool Ray2D::contains(Vector2D<float> point) const
    {
        bool contemNaReta = fullLine.contains(point);
        bool contemNaSemireta = Vector2D<float>::innerProd(point - origin , fullLine.getDir()) > 0;
        return contemNaReta && contemNaSemireta;
    }

    Vector2D<float>* Ray2D::intersection(Line2D line) const
    {
        Vector2D<float> *intersec = fullLine.intersection(line);

        if(intersec!= nullptr && contains(*intersec))
            return intersec;
        else
            return nullptr;
    }

    Vector2D<float>* Ray2D::intersection(SegLine2D segline) const
    {
        Vector2D<float> *intersec = fullLine.intersection(segline);

        if(intersec != nullptr && contains(*intersec) )
            return intersec;
        else
            return nullptr;
    }


}