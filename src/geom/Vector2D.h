//
// Created by ericpqmor on 19/05/17.
//

#ifndef CINAC_SIMULATOR_VECTOR2D_H
#define CINAC_SIMULATOR_VECTOR2D_H

#include <cmath>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <SFML/Graphics.hpp>

namespace cinac {

/** This class represents a 2-vector and a 2-point*/
    template<class V>
    class Vector2D {
    public:

        Vector2D<V>() :
                x(V()), y(V()), r(V()), a(V()) {
        }

        Vector2D<V>(V x, V y)
                :   x(x), y(y),
                    r(sqrt(x * x + y * y)),
                    a(atan2(y, x) < 0 ? 2 * M_PI - atan2(y, x) : atan2(y, x))
        {
        }

        Vector2D<V>(const sf::Vector2<V> &sfVector)
        {
            *this = Vector2D<V>(sfVector.x, sfVector.y);
        }

        Vector2D<V> &operator=(const Vector2D<V> &other) {
            x = other.getX();
            y = other.getY();
            r = other.getR();
            a = other.getA();
            return *this;
        }

        Vector2D<V>(const Vector2D<V> &other) {
            *this = other;
        }

        Vector2D<V> &operator+=(const Vector2D<V> &other) {
            x += other.getX();
            y += other.getY();
            r = sqrt(x * x + y * y);
            a = atan2(y, x) < 0 ? 2 * M_PI - atan2(y, x) : atan2(y, x);
            return *this;
        }

        Vector2D<V> &operator+=(const sf::Vector2<V> &sfOther) {
            x += sfOther.x;
            y += sfOther.y;
            r = sqrt(x * x + y * y);
            a = atan2(y, x) < 0 ? 2 * M_PI - atan2(y, x) : atan2(y, x);
            return *this;
        }

        Vector2D<V> &operator-=(const Vector2D<V> &other) {
            x -= other.getX();
            y -= other.getY();
            r = sqrt(x * x + y * y);
            a = atan2(y, x) < 0 ? 2 * M_PI - atan2(y, x) : atan2(y, x);
            return *this;
        }

        Vector2D<V> &operator-=(const sf::Vector2<V> &sfOther) {
            x -= sfOther.x;
            y -= sfOther.y;
            r = sqrt(x * x + y * y);
            a = atan2(y, x) < 0 ? 2 * M_PI - atan2(y, x) : atan2(y, x);
            return *this;
        }

        Vector2D<V> &operator*=(const V &factor) {
            x *= factor;
            y *= factor;
            r *= factor>0?factor:-factor;
            return *this;
        }

        Vector2D<V> &operator/=(const V &factor) {
            if (factor == 0)
                return *this;
            x /= factor;
            y /= factor;
            r /= factor>0?factor:-factor;
            return *this;
        }

        Vector2D<V> operator+(const Vector2D<V> &other) const {
            return Vector2D<V>(*this) += other;
        }

        Vector2D<V> operator+(const sf::Vector2<V> &other) const {
            return Vector2D<V>(*this) += other;
        }

        Vector2D<V> operator-(const Vector2D<V> &other) const {
            return Vector2D<V>(*this) -= other;
        }

        Vector2D<V> operator-(const sf::Vector2<V> &other) const {
            return Vector2D<V>(*this) -= other;
        }

        Vector2D<V> operator*(const V &factor) const {
            return Vector2D<V>(*this) *= factor;
        }

        Vector2D<V> operator/(const V &factor) const {
            return Vector2D<V>(*this) /= factor;
        }

        bool operator==(const Vector2D<V> &other) const {
            return (x == other.getX() &&  y == other.getY());
        }

        bool operator!=(const Vector2D<V> &other) const {
            return !(*this == other);
        }

        V getX() const { return x; }

        void setX(V newX) {
            x = newX;
            r = x * x + y * y;
            a = atan2(y, x) < 0 ? 2 * M_PI - atan2(y, x) : atan2(y, x);
        }

        V getY() const { return y; }

        void setY(V newY) {
            y = newY;
            r = x * x + y * y;
            a = atan2(y, x) < 0 ? 2 * M_PI - atan2(y, x) : atan2(y, x);
        }

        V getR() const { return r; }

        void setR(V newR) {
            r = newR;
            x = r * cos(a);
            y = r * sen(a);
        }

        V getA() const { return a; }

        void setA(V newA) {
            a = newA;
            x = r * cos(a);
            y = r * sen(a);
        }

        V abs() const {
            return (V) sqrt(((double) x) * x + ((double) y) * y);
        }

        V squareAbs() const {
            return (x * x) + (y * y);
        }

        Vector2D<V> normalize() {
            const V length = Vector2D::abs();
            if (length == 0)
                return *this;
            return *this /= length;
        }

        float distance(Vector2D<V> other) const {
            float dx = this->x - other.getX();
            double dy = this->y - other.getY();
            return sqrt(dx * dx + dy * dy);
        }

        V &operator[](int i) {
            return (&x)[i];
        }

        const V &operator[](int i) const {
            return (&x)[i];
        }

        //
        //  Rotates a point around a origin in theta degrees in counter-clockwise direction
        //
        static Vector2D<V> rotateAroundOrigin(Vector2D<V> point, Vector2D<V> origin, double theta) {
            Vector2D<V> rotated(point-origin);

            double s = sin(theta*M_PI/180);
            double c = cos(theta*M_PI/180);

            double xNew = (rotated.getX()) * c + (rotated.getY()) * s;
            double yNew = -(rotated.getX()) * s + (rotated.getY()) * c;

            rotated.setX(xNew);
            rotated.setY(yNew);

            rotated += origin;

            return rotated;
        }

        static double innerProd(const Vector2D<V> &v1, const Vector2D<V> &v2) {
            return v1.getX() * v2.getX() + v1.getY() * v2.getY();
        }

        static double crossProd(const Vector2D<V> &v1, const Vector2D<V> &v2) {
            return v1.getX() * v2.getY() - v2.getX() * v1.getY();
        }


    private:
        V x, y, r, a;
    };

}
#endif //CINAC_SIMULATOR_VECTOR2D_H
