#include "Manager.h"

int main() {

    sf::RenderWindow window(sf::VideoMode(800, 600), "CiNAC Simulator");
    sf::RenderWindow auxWindow(sf::VideoMode(480, 120), "Vibrations percentages", sf::Style::Titlebar | sf::Style::Resize);

    Manager manager(&window, &auxWindow);
    manager.run();

    KalmanFilterTester kalmanFilterTester;
    kalmanFilterTester.test();

    return 0;
}
