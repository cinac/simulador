//
// Created by igor on 23/05/17.
//

#ifndef CINAC_SIMULATOR_MANAGER_H
#define CINAC_SIMULATOR_MANAGER_H

#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <SFML/Graphics.hpp>
#include "Blindman.h"
#include "Obstacles.h"
#include "geom/Vector2D.h"
#include "KalmanFilter/KalmanFilter.h"

class Manager {
public:
    Manager(sf::RenderWindow *W, sf::RenderWindow *auxW);
    ~Manager();
    void run();

//    KalmanFilterTester kalmanFilterTester;

private:
    /*
     *  Analyses Mouse input and updates blindman's position
     */
    void handleUserInput();

    /*
     *  Updates Blindman object position.
     *      Returns true if could move walker to a valid position
     *      Returns false if couldn't
     */
    bool translateBlindmanPosition(sf::Vector2f newPosition);
    void rotateBlindmanPosition(bool clockwise);

    /*
     *  Update vibration percentages
     */
    void updatePercentages();

    void updateGraphics();
    void drawWindow();
    void drawAuxWindow(std::string &newText);

    sf::RenderWindow *window;
    sf::RenderWindow *auxWindow;

    Blindman *walker;
    Obstacles *obstacles;
    sf::Font font;
    std::vector<int> percentages;

    //
    //  Wait time between rotations of the Blindman
    //
    int waitTime;
    void updateWaitTime(int state);
};


#endif //CINAC_SIMULATOR_MANAGER_H
