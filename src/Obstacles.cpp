#include "Obstacles.h"

#define DEBUG_OBSTACLES

//Construtor for the obstacles object
Obstacles::Obstacles()
{
  //Loads the texture
  if (!this->wallTexture.loadFromFile("../res/images/wall.png"))
    std::cout << "Could not load Texture for wall." << std::endl;

  //Sets texture to the object
    this->wallSprite.setTexture(this->wallTexture);

    //Fills the walls vector
    this->walls = createWalls(this->wallSprite);
}

//Destructor for the obstacles object
Obstacles::~Obstacles()
{

}

std::vector< sf::Sprite > Obstacles::createWalls(sf::Sprite wall)
{
  //Creates the walls vector
    std::vector< sf::Sprite > walls;

    //Locate the new object and push to vector
    wall.setPosition(sf::Vector2f(5,50));
    walls.push_back(wall);

    wall.setPosition(sf::Vector2f(5,280));
    walls.push_back(wall);

    wall.setPosition(sf::Vector2f(200,0));
    walls.push_back(wall);

    wall.setPosition(sf::Vector2f(600,0));
    walls.push_back(wall);

    wall.setPosition(sf::Vector2f(200,370));
    walls.push_back(wall);

    wall.setPosition(sf::Vector2f(600,370));
    walls.push_back(wall);

#ifdef DEBUG_OBSTACLES

    debugObst.assign(walls.size(), std::vector<sf::CircleShape>( 4, sf::CircleShape(4,80) ) );
    for(int i=0; i<(int)debugObst.size(); i++){
        sf::Vector2f basePos(walls[i].getPosition()-sf::Vector2f(4,4));
        debugObst[i][0].setPosition(basePos);
        debugObst[i][1].setPosition(basePos.x, basePos.y + wall.getLocalBounds().height);
        debugObst[i][2].setPosition(basePos.x + wall.getLocalBounds().width, basePos.y+ wall.getLocalBounds().height);
        debugObst[i][3].setPosition(basePos.x + wall.getLocalBounds().width, basePos.y);

        for(int j=0; j<(int)debugObst[i].size(); j++) {
            debugObst[i][j].setFillColor(sf::Color::Cyan);
        }
    }
#endif


    return walls;
}
