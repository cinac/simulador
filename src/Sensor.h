
#include <algorithm>
#include <SFML/Graphics.hpp>
#include "geom/Ray2D.h"
#include "geom/Vector2D.h"

class Blindman;

#define DEBUG_SENSOR_DISTANCE

class Sensor{
private:

    //
    //  Local Position of the Sensor( on the border of Blindman)
    //  and Blindman's Center with respect to the window
    //
    cinac::Vector2D<float> localPosition;
    cinac::Vector2D<float> blindCenter;

    //
    //  Ray structure
    //
    cinac::Ray2D ray;

    //
    //  Distance obtaines by sensor
    //
    float dist;
    const float INF = 2000;

    //
    //  Properties for graphical display only
    //
    float shapeRadius;
    sf::CircleShape graphicalUtil;

public:

    //
    //  Basic constructor. Receives blindman's body center position,
    //      sensor position and sensor radius
    //
    Sensor(cinac::Vector2D<float> blindCenter, cinac::Vector2D<float> pos, float radius);

    ~Sensor();

    sf::CircleShape &getShape() { return graphicalUtil; }

    cinac::Vector2D<float> position() const { return localPosition; }

    //
    //  Translates the sensor about some displacement
    //
    void translate(sf::Vector2f displacement);

    //
    //  Rotates the sensor around a pivot in counter-clockwise direction
    //
    void rotate(cinac::Vector2D<float> pivot, bool clockwise);


    //
    //  Updates the ray geometrical structure after moving sensor
    //
    void updateRay();

    //
    //  Sends a ray from the sensor to return minimal distance from any obstacle
    //  Updates dist with this minimal distance
    //
    float sendRay(sf::RenderWindow * w,  std::vector<cinac::Polygon2D> &obstacles, int flag, Blindman *blindman);

    float getDist() const { return dist; }

    cinac::Ray2D getRay() const { return ray; }
};
